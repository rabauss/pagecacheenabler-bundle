<?php

/*
 * This file is part of Pliigo.
 *
 * Copyright (c) 2018 Johannes Pichler
 *
 * @license LGPL-3.0+
 */

namespace Pliigo\PliigoPageCacheEnabler;

use Pliigo\PliigoPageCacheEnabler\DependencyInjection\PageCacheEnablerExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class PliigoPageCacheEnablerBundle extends Bundle
{

    /**
     * Builds the bundle.
     *
     * It is only ever called once when the cache is empty.
     *
     * This method can be overridden to register compilation passes,
     * other extensions, ...
     *
     * @param ContainerBuilder $container A ContainerBuilder instance
     */
    public function build(ContainerBuilder $container)
    {
    }
    /**
     * {@inheritdoc}
     */
    public function getContainerExtension()
    {
        return new PageCacheEnablerExtension();
    }

}